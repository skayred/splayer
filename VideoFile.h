/*
 * VideoFile.h
 *
 *  Created on: 03 нояб. 2013 г.
 *      Author: sk_
 */

#ifndef VIDEOFILE_H_
#define VIDEOFILE_H_

extern "C" {
	#include <libavcodec/avcodec.h>
	#include <libavformat/avformat.h>
	#include <libswscale/swscale.h>
}

#include <mutex>
#include <queue>
#include <unistd.h>

#include "Util.h"
#include "Syncronizer.h"

class FrameHolder {
public:
	FrameHolder(AVFrame *frame, double presentationTime) {
		this->frame = frame;
		this->presentationTime = presentationTime;
	}

	AVFrame *getFrame() {
		return frame;
	}

	double getPresentationTime() {
		return presentationTime;
	}

	void freeFrame() {
		avcodec_free_frame(&frame);
	}

	~FrameHolder() {
		freeFrame();
	}
private:
	AVFrame *frame;
	double presentationTime;
};

//	Инкапсулирует всю работу с видеофайлом - инициализация стримов, поллинг фреймов и прочая фигня
class VideoFile {
public:
	uint8_t audioBuffer[(MAX_AUDIO_FRAME_SIZE * 3) / 2];
	int audioIndex;
	int audioBufferSize;

	VideoFile(const char* filename) {
		this->filename = filename;
		fileEnded = false;

		av_register_all();
		formatContext = NULL;
		avformat_open_input(&formatContext, filename, NULL, NULL);
		avformat_find_stream_info(formatContext, NULL);
		av_dump_format(formatContext, 0, filename, 0);

		findStreams();

		videoQueue = new std::queue<AVPacket>();
		audioQueue = new std::queue<AVPacket>();
	}

	void setSync(Syncronizer *sync) {
		this->sync = sync;
	}

	void pollFrame() {
		AVPacket avpacket;
		bool videoFrameFound = false;
		bool audioFrameFound = false;

		while (av_read_frame(getFormatContext(), &avpacket) >= 0) {
			if (avpacket.stream_index == videoStreamID) {
				// Video stream packet
				checkVideoQueue();

				videoMutex.lock();
				videoQueue->push(avpacket);
				videoMutex.unlock();

				videoFrameFound = true;
			} else if (avpacket.stream_index == audioStreamID) {
				audioMutex.lock();
				audioQueue->push(avpacket);
				audioMutex.unlock();

				audioFrameFound = true;
			} else {
				av_free_packet(&avpacket);

				continue;
			}

			if (videoFrameFound && audioFrameFound) {
				return;
			}
		}

		fileEnded = true;
	}

	bool hasVideoFrame() {
		int size = getVideoQueueLength();

		return ((size > 0) || (!fileEnded));
	}

	FrameHolder getVideoFrame() {
		int frameFinished = 0;
		AVFrame *frame = avcodec_alloc_frame();
		AVPacket packet;

		while (!frameFinished) {
			videoMutex.lock();
			packet = videoQueue->front();
			videoQueue->pop();
			videoMutex.unlock();

			avcodec_decode_video2(getCodecContext(), frame, &frameFinished, &packet);
		}

		double presentationTimestamp;
		if(packet.dts == AV_NOPTS_VALUE && frame->opaque && *(uint64_t*)frame->opaque != AV_NOPTS_VALUE) {
			presentationTimestamp = *(uint64_t *)frame->opaque;
		} else if(packet.dts != AV_NOPTS_VALUE) {
			presentationTimestamp = packet.dts;
		} else {
			presentationTimestamp = 0;
		}
		presentationTimestamp *= av_q2d(formatContext->streams[videoStreamID]->time_base);
		double frameDelay = av_q2d(formatContext->streams[videoStreamID]->time_base);
		  /* if we are repeating a frame, adjust clock accordingly */
		frameDelay += frame->repeat_pict * (frameDelay * 0.5);

		return FrameHolder(frame, presentationTimestamp + frameDelay);
	}

	int getAudioBuffer(int length) {
		if ((audioBufferSize - audioIndex) > 0) {
			return std::min((audioBufferSize - audioIndex), length);
		}

		audioMutex.lock();
		int queueSize = audioQueue->size();
		audioMutex.unlock();

		while (queueSize > 0) {
			audioMutex.lock();
			AVPacket packet = audioQueue->front();
			audioQueue->pop();
			queueSize = audioQueue->size();
			audioMutex.unlock();

			AVFrame *frameBuffer = avcodec_alloc_frame();
			int frameFinished;
			int frameLength = avcodec_decode_audio4(audioCodecContext, frameBuffer, &frameFinished, &packet);

			if(frameLength < 0) {
				/* if error, skip frame */
				continue;
			}

			if (frameFinished) {
				audioBufferSize =
						av_samples_get_buffer_size
						(
								NULL,
								getAudioStream()->codec->channels,
								frameBuffer->nb_samples,
								getAudioStream()->codec->sample_fmt,
								1
						);

				memcpy(audioBuffer, frameBuffer->data[0], audioBufferSize);
				audioIndex = 0;

				if(packet.pts != AV_NOPTS_VALUE) {
					long double audioPTS = (av_q2d(getAudioStream()->time_base) * packet.pts);

					sync->setAudioPTS(audioPTS);
				}

				return std::min(length, audioBufferSize);
			}
		}

		return 0;
	}

	bool hasAudioStream() {
		return audioStreamID != -1;
	}

	AVStream *getAudioStream() {
		return formatContext->streams[audioStreamID];
	}

	//	Getters
	SDL_AudioSpec getAudioFormat() {
		SDL_AudioSpec audioFormatObject;

		audioFormatObject.freq = audioFrequency;
		audioFormatObject.format = audioFormat;
		audioFormatObject.channels = audioChannels;
		audioFormatObject.samples = audioSamples;

		return audioFormatObject;
	}

	const char *getFilename() { return filename; }
	AVCodecContext *getCodecContext() { return videoCodecContext; }
	AVFormatContext *getFormatContext() { return formatContext; }
	int getVideoStreamID() { return videoStreamID; }
	bool isFileEnded() { return fileEnded; }
private:
	const char *filename;
	Syncronizer *sync;
	int videoStreamID;
	int audioStreamID;
	AVCodecContext* videoCodecContext;
	AVCodecContext* audioCodecContext;
	AVFormatContext* formatContext;
	AVCodec* videoCodec;
	AVCodec* audioCodec;
	bool fileEnded;

	int audioFrequency;
	Uint16 audioFormat;
	Uint8 audioChannels;
	Uint8 audioSilence;
	Uint16 audioSamples;

	std::mutex videoMutex;
	std::mutex audioMutex;
	std::queue<AVPacket> *videoQueue;
	std::queue<AVPacket> *audioQueue;

	void checkVideoQueue() {
		unsigned int size = getVideoQueueLength();

		while (size >= MAX_VIDEO_QUEUE_SIZE) {
			usleep(0.01 * 1000000);
			size = getVideoQueueLength();

			std::cout << "Overflow" << std::endl;
		}
	}

	int getVideoQueueLength() {
		videoMutex.lock();
		int size = videoQueue->size();
		videoMutex.unlock();

		return size;
	}

	void findStreams() {
		// Find the first video stream
		for (unsigned int i = 0; i < formatContext->nb_streams; ++i) {
			if (formatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO) {
				videoStreamID = i;
			}
			if(formatContext->streams[i]->codec->codec_type==AVMEDIA_TYPE_AUDIO) {
				audioStreamID = i;
			}
		}

		videoCodecContext = formatContext->streams[videoStreamID]->codec;
		audioCodecContext = formatContext->streams[audioStreamID]->codec;

		audioFrequency = audioCodecContext->sample_rate;
		audioFormat = AUDIO_U8;
		audioChannels = audioCodecContext->channels;
		audioSilence = 0;
		audioSamples = SDL_AUDIO_BUFFER_SIZE;

		videoCodec = avcodec_find_decoder(videoCodecContext->codec_id);
		audioCodec = avcodec_find_decoder(audioCodecContext->codec_id);

		videoCodecContext->get_buffer = getBuffer;
		videoCodecContext->release_buffer = releaseBuffer;

		openCodec(videoCodecContext, videoCodec);
		openCodec(audioCodecContext, audioCodec);
	}

	void openCodec(AVCodecContext *context, AVCodec *codec) {
		avcodec_alloc_context3(codec);

		if (avcodec_open2(context, codec, NULL) < 0) {
			fprintf(stderr, "ffmpeg: Unable to allocate codec context\n");
		} else {
			printf("Codec initialized\n");
		}
	}
};

#endif /* VIDEOFILE_H_ */
