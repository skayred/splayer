/*
 * AudioStreamer.h
 *
 *  Created on: 03 нояб. 2013 г.
 *      Author: sk_
 */

#ifndef AUDIOSTREAMER_H_
#define AUDIOSTREAMER_H_

#include "SDL2/SDL.h"

#include "Syncronizer.h"

void audioCallbackWrapper(void *userdata, uint8_t *stream, int len);

class AudioStreamer {
public:
	AudioStreamer(VideoFile *video, Syncronizer *sync) {
		this->video = video;
		this->sync = sync;
	}

	void playAudio() {
		SDL_AudioSpec audioFormat = video->getAudioFormat();
		audioFormat.callback = audioCallbackWrapper;
		audioFormat.userdata = this;

		if (SDL_OpenAudio(&audioFormat, NULL) < 0) {
			fprintf(stderr, "Unable to open audio: %s\n", SDL_GetError());
		}

		SDL_PauseAudio(0);
	}

	void audioCallback(uint8_t *stream, int len) {
		while (len > 0) {
			int actualLength = video->getAudioBuffer(len);
			synchronizeAudio((int16_t *)video->audioBuffer, actualLength, 0);

			memcpy(stream, (uint8_t *) video->audioBuffer + video->audioIndex, actualLength);

			stream += actualLength;
			video->audioIndex += actualLength;
			len -= actualLength;

			if (sync->isQuit()) {
				//	Drop the bass
				SDL_PauseAudio(1);
				return;
			}
		}
	}
private:
	VideoFile *video;
	Syncronizer *sync;

	int synchronizeAudio(short *samples, int samplesSize, double pts) {
		int n;
		double refClock;

		n = 2 * video->getAudioStream()->codec->channels;

		double diff, avg_diff;
		int wanted_size, min_size, max_size /*, nb_samples */;

		refClock = sync->getVideoClock();
		diff = sync->getAudioClock(video->audioBufferSize, video->audioIndex) - refClock;

		if(diff < AV_NOSYNC_THRESHOLD) {
			// accumulate the diffs
			sync->audioDiffCum = diff + sync->audioDiffAvgCoef * sync->audioDiffCum;
			if(sync->audioDiffAvgCount < AUDIO_DIFF_AVG_NB) {
				sync->audioDiffAvgCount++;
			} else {
				avg_diff = sync->audioDiffCum * (1.0 - sync->audioDiffAvgCoef);
				if(fabs(avg_diff) >= sync->audioDiffThreshold) {
					wanted_size = samplesSize + ((int)(diff * video->getAudioStream()->codec->sample_rate) * n);
					min_size = samplesSize * ((100 - SAMPLE_CORRECTION_PERCENT_MAX) / 100);
					max_size = samplesSize * ((100 + SAMPLE_CORRECTION_PERCENT_MAX) / 100);
					if(wanted_size < min_size) {
						wanted_size = min_size;
					} else if (wanted_size > max_size) {
						wanted_size = max_size;
					}
					if(wanted_size < samplesSize) {
						/* remove samples */
						samplesSize = wanted_size;
					} else if(wanted_size > samplesSize) {
						uint8_t *samples_end, *q;
						int nb;

						/* add samples by copying final sample*/
						nb = (samplesSize - wanted_size);
						samples_end = (uint8_t *)samples + samplesSize - n;
						q = samples_end + n;
						while(nb > 0) {
							memcpy(q, samples_end, n);
							q += n;
							nb -= n;
						}
						samplesSize = wanted_size;
					}
				}
			}
		} else {
			/* difference is TOO big; reset diff stuff */
			sync->audioDiffAvgCount = 0;
			sync->audioDiffCum = 0;
		}

		return samplesSize;
	}
};

void audioCallbackWrapper(void *userdata, uint8_t *stream, int len) {
	AudioStreamer* streamer = (AudioStreamer *)userdata;

	streamer->audioCallback(stream, len);
}

#endif /* AUDIOSTREAMER_H_ */
