/*
 * Util.h
 *
 *  Created on: 27 РѕРєС‚. 2013 Рі.
 *      Author: sk_
 */

#ifndef UTIL_H_
#define UTIL_H_

extern "C" {
	#include <libavcodec/avcodec.h>
	#include <libavformat/avformat.h>
	#include <libswscale/swscale.h>
}

#define SDL_AUDIO_BUFFER_SIZE 512
#define MAX_AUDIO_FRAME_SIZE 192000

#define MAX_VIDEO_QUEUE_SIZE 100

uint64_t global_video_pkt_pts = AV_NOPTS_VALUE;

int getBuffer(struct AVCodecContext *c, AVFrame *pic) {
	int ret = avcodec_default_get_buffer(c, pic);
	uint64_t *pts = (uint64_t *) av_malloc(sizeof(uint64_t));
	*pts = global_video_pkt_pts;
	pic->opaque = pts;

	return ret;
}
void releaseBuffer(struct AVCodecContext *c, AVFrame *pic) {
	if(pic) {
		av_freep(&pic->opaque);
	}

	avcodec_default_release_buffer(c, pic);
}


#endif /* UTIL_H_ */
