/*
 * VideoStreamer.h
 *
 *  Created on: 03 нояб. 2013 г.
 *      Author: sk_
 */

#ifndef VIDEOSTREAMER_H_
#define VIDEOSTREAMER_H_

#include <SDL2/SDL.h>
#include <unistd.h>

#include "Syncronizer.h"

class VideoStreamer {
public:
	VideoStreamer(VideoFile *video, SDL_Renderer *renderer, Syncronizer *sync) {
		this->video = video;
		this->renderer = renderer;
		this->sync = sync;

		this->texture = SDL_CreateTexture(renderer,
		    		SDL_PIXELFORMAT_IYUV,
					SDL_TEXTUREACCESS_STREAMING,
					video->getCodecContext()->width,
					video->getCodecContext()->height);

		PictureMutex = SDL_CreateMutex();
		PictureReadyCond = SDL_CreateCond();
		PictureReady = false;
	}

	void start() {
		while (video->hasVideoFrame()) {
			FrameHolder currentFrame = video->getVideoFrame();

			usleep(sync->getFrameDelay(currentFrame.getPresentationTime()));

			preparePicture(currentFrame.getFrame(), video->getCodecContext());

			SDL_RenderClear(renderer);
			SDL_RenderCopy(renderer, texture, NULL, NULL);
			SDL_RenderPresent(renderer);

			sync->setVideoPTS(currentFrame.getPresentationTime());

			if (sync->isQuit()) {
				return;
			}
		}
	}
private:
	VideoFile *video;
	SDL_Renderer *renderer;
	SDL_Texture *texture;
	Syncronizer *sync;
	SDL_mutex* PictureMutex;
	SDL_cond* PictureReadyCond;
	bool PictureReady;

	uint8_t * toYUV420(AVFrame* frame, AVCodecContext *codecContext) {
		static int numPixels = avpicture_get_size (
				PIX_FMT_YUV420P,
				codecContext->width,
				codecContext->height);
		static uint8_t * buffer = (uint8_t *) malloc (numPixels);

		//Set context for conversion
		static struct SwsContext *swsContext = sws_getCachedContext (
			swsContext,
			codecContext->width,
			codecContext->height,
			codecContext->pix_fmt,
			codecContext->width,
			codecContext->height,
			PIX_FMT_YUV420P,
			SWS_BILINEAR,
			NULL,
			NULL,
			NULL
		);

		AVFrame* frameYUV420P = avcodec_alloc_frame();
		avpicture_fill(
				(AVPicture *) frameYUV420P,
				buffer,
				PIX_FMT_YUV420P,
				codecContext->width,
				codecContext->height);

		// Convert the image into YUV format that SDL uses
		sws_scale(swsContext,
				frame->data, frame->linesize,
					0, codecContext->height,
					frameYUV420P->data,	frameYUV420P->linesize );

		return buffer;
	}

	void preparePicture(AVFrame *frame, AVCodecContext *codecContext) {
		SDL_LockMutex(PictureMutex);
	//	while (PictureReady)
	//		SDL_CondWait(PictureReadyCond, PictureMutex);
		SDL_UnlockMutex(PictureMutex);

		int pitch = codecContext->width * SDL_BYTESPERPIXEL(SDL_PIXELFORMAT_IYUV);
		uint8_t * buffer = toYUV420(frame, codecContext);
		SDL_UpdateTexture(texture, NULL, buffer, pitch);

		SDL_LockMutex(PictureMutex);
		PictureReady = true;
		SDL_CondSignal(PictureReadyCond);
		SDL_UnlockMutex(PictureMutex);
	}
};


#endif /* VIDEOSTREAMER_H_ */
