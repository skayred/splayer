/*
 * Decoder.h
 *
 *  Created on: 03 нояб. 2013 г.
 *      Author: sk_
 */

#ifndef DECODER_H_
#define DECODER_H_

#include <SDL2/SDL.h>
#include <thread>

#include "Syncronizer.h"

class Decoder;

void decodingWork(Decoder *decoder, VideoFile *video);

class Decoder {
public:
	Decoder(VideoFile *video, Syncronizer *sync) {
		this->video = video;
		this->sync = sync;
		this->stopFlag = false;
	}

	void startDecoding() {
		decodingThread = std::thread(decodingWork, this, video);
	}

	void stopDecoding() {
		stopFlag = true;
		decodingThread.join();
	}

	bool getStopFlag() { return stopFlag; }
	bool isQuit() { return sync->isQuit(); }
private:
	VideoFile *video;
	std::thread decodingThread;
	Syncronizer *sync;
	bool stopFlag;
};

void decodingWork(Decoder *decoder, VideoFile *video) {
	while (!video->isFileEnded()) {
		if (decoder->getStopFlag()) {
			return;
		}

		video->pollFrame();

		if (decoder->isQuit()) {
			return;
		}
	}
}

#endif /* DECODER_H_ */
