extern "C" {
	#include <libavcodec/avcodec.h>
	#include <libavformat/avformat.h>
	#include <libswscale/swscale.h>
}

#include <SDL2/SDL.h>
#include <stdio.h>

#include <iostream>

#include "VideoFile.h"
#include "Decoder.h"
#include "VideoStreamer.h"
#include "AudioStreamer.h"
#include "Syncronizer.h"

int main(int argc, char * argv[]) {
	if (argc < 2) {
		printf("Usage: %s filename\n", argv[0]);
		return 0;
	}

	int err;
	// Init SDL with video support
	err = SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_TIMER);
	if (err < 0) {
		fprintf(stderr, "Unable to init SDL: %s\n", SDL_GetError());
		return -1;
	}

	VideoFile *video = new VideoFile(argv[1]);

	int w = 640;
	int h = 480;
	SDL_Window *screen = SDL_CreateWindow(video->getFilename(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, w, h, 0);
	SDL_Renderer *renderer = SDL_CreateRenderer(screen, -1, 0);

	Syncronizer *syncronizr = new Syncronizer(video->getAudioStream()->codec->channels, video->getAudioStream()->codec->sample_rate);
	Decoder *decoder = new Decoder(video, syncronizr);
	video->setSync(syncronizr);

	decoder->startDecoding();

	usleep(0.5 * 1000000);

	VideoStreamer *videoStreamer = new VideoStreamer(video, renderer, syncronizr);
	AudioStreamer *audioStreamer = new AudioStreamer(video, syncronizr);
	if (video->hasAudioStream()) {
		audioStreamer->playAudio();
	}

	videoStreamer->start();

	return 0;
}
