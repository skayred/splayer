/*
 * Syncronizer.h
 *
 *  Created on: 03 нояб. 2013 г.
 *      Author: sk_
 */

#ifndef SYNCRONIZER_H_
#define SYNCRONIZER_H_

#define AV_SYNC_THRESHOLD 0.01
#define AV_NOSYNC_THRESHOLD 10.0

#define SAMPLE_CORRECTION_PERCENT_MAX 10

#define AUDIO_DIFF_AVG_NB 20

class Syncronizer {
public:
	double audioDiffCum;
	double audioDiffAvgCoef;
	double audioDiffThreshold;
	int audioDiffAvgCount;

	Syncronizer(int audioChannelsNumber, int audioSampleRate) {
		lastVideoPTS = -1;
		lastAudioPTS = -1;

		this->audioChannelsNumber = audioChannelsNumber;
		this->audioSampleRate = audioSampleRate;
	}

	long double getFrameDelay(long double presentationTime) {
		if (lastVideoPTS != -1) {
			long double videoErrorDelay = (presentationTime - lastVideoPTS) - (getCurrentTime() - lastVideoTimestamp);

			return ((presentationTime - lastVideoPTS) * 1000000 + videoErrorDelay);
		}

		return 0;
	}

	bool isQuit() {
		if (!quitGot) {
			SDL_Event event;
			SDL_PollEvent(&event);
			switch(event.type) {
				case SDL_QUIT:
					quitGot = true;
					return true;
				default:
					break;
			}

			return false;
		} else {
			return true;
		}
	}

	void setVideoPTS(long double videoPST) {
		lastVideoPTS = videoPST;
		lastVideoTimestamp = getCurrentTime();
	}

	void setAudioPTS(long double audioPTS) {
		lastAudioPTS = audioPTS;
	}

	long double getCurrentTime() {
		return av_gettime();
	}

	double getVideoClock() {
		double delta;

		delta = (getCurrentTime() - lastVideoPTS) / 1000000.0;

		return lastVideoPTS + delta;
	}

	double getAudioClock(int audioBufferSize, int audioIndex) {
		double pts;
		int hwBufSize, bytesPerSec, n;

		pts = lastAudioPTS;
		hwBufSize = audioBufferSize - audioIndex;
		bytesPerSec = 0;
		n = audioChannelsNumber * 2;
		bytesPerSec = audioSampleRate * n;

		if(bytesPerSec) {
			pts -= (double) hwBufSize / bytesPerSec;
		}

		return pts;
	}
private:
	long double lastAudioPTS;
	long double lastVideoPTS;
	long double lastVideoTimestamp;

	int audioChannelsNumber;
	int audioSampleRate;
	bool quitGot;
};


#endif /* SYNCRONIZER_H_ */
